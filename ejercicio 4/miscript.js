function loadEvents(){
    document.getElementById("mostrarUsuarios").addEventListener('click',mostrarUsuarios);
    document.getElementById("mostrarMisiones").addEventListener('click',mostrarMisiones);
    document.getElementById("mostrarUsuarioPorId").addEventListener('click',mostrarUsuarioPorId);
}

function mostrarUsuarios(){
    window.location="./usuarios.php";
}

function mostrarMisiones(){
    window.location="./misiones.php?var=" + document.getElementById('inputId').value;
}

function mostrarUsuarioPorId(){
    window.location="./usuarioPorId.php?var=" + document.getElementById('inputId').value;
}