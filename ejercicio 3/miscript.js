function loadEvents(){
    document.getElementById("mostrarUsuarios").addEventListener('click',mostrarUsuarios);
    document.getElementById("mostrarMisiones").addEventListener('click',mostrarMisiones);
    document.getElementById("mostrarUsuariosConScore").addEventListener('click',mostrarUsuariosConScore);

}

function mostrarUsuarios(){
    window.location="./usuarios.php";
}

function mostrarMisiones(){
    window.location="./misiones.php";
}
function mostrarUsuariosConScore(){
    window.location="./score.php";
}