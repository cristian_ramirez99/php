-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-02-2021 a las 13:35:23
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `missions`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quests`
--

CREATE TABLE `quests` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `num_vots` int(11) NOT NULL,
  `mitjana` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `quests`
--

INSERT INTO `quests` (`id`, `descripcion`, `num_vots`, `mitjana`) VALUES
(1, 'Mata a todos', 5, '2.30'),
(2, 'Secuestra a Manolo', 7, '3.20'),
(3, 'Jesus te ama', 9, '2.50'),
(4, 'Mata al homonculo', 8, '3.70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `score` int(11) NOT NULL,
  `nick` varchar(40) NOT NULL,
  `apellido` varchar(40) NOT NULL,
  `passwd` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `score`, `nick`, `apellido`, `passwd`) VALUES
(1, 'jhonny', 12, '', '', ''),
(2, 'jordi', 12, '', '', ''),
(3, 'manolo', 6, '', '', ''),
(4, 'oscar', 1, '', '', ''),
(6, 'random', 0, '', '', ''),
(7, 'jesucristo', 10, '', '', ''),
(8, 'bobo', 0, '', '', ''),
(9, 'parpal', 2, '', '', ''),
(10, 'grau', 0, '', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `quests`
--
ALTER TABLE `quests`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `quests`
--
ALTER TABLE `quests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
